const axios = require('axios')
const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()

router.use(bodyParser.json())

router.get('/api/get-user-information', async (req, res) => {
    if (req.session.access_token) {
        try {
            const response = await axios.get('https://discord.com/api/v8/users/@me', {
                headers: { Authorization: `Bearer ${req.session.access_token}` }
            })
            res.send(response.data);
        } catch (e) {
            console.log(e)
            res.send(e)
            throw e;
        }
    }
})

module.exports = router;