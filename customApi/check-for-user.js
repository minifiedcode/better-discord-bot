// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
const { Pool } = require('pg')
const pool = new Pool()
require('dotenv').config()

async function createUser(id, nickname) {
  const client = await pool.connect()
  try {
    const createUserQueryTxt = 'INSERT INTO users(id, server_id, nickname, level, xp) VALUES($1, $2, $3, 1, 15)'
    const createUserQueryVals = [id, '1', nickname]
    const res = await client.query(createUserQueryTxt, createUserQueryVals)
  } catch (e) {
    console.log(e)
  } finally {
    client.release()
  }
}

function checkForLevelUp(xp, level) {
  const newLevelXp = process.env.NEXT_PUBLIC_XP_BASE * (process.env.NEXT_PUBLIC_XP_MULTIPLIER * level)
  let overflowXp = 0
  let didLevelUp = false
  if (xp > newLevelXp && !didLevelUp) {
    didLevelUp = true
    overflowXp = xp - newLevelXp
    xp = 0
  }

  if (xp === newLevelXp && !didLevelUp) {
    xp = 0
    didLevelUp = true
  }

  if (didLevelUp) {
    return xp + overflowXp
  }

  return false
}

async function addXp(id) {
  console.log('adding xp')
  const client = await pool.connect()
  try {
    const grabXpQueryTxt = 'SELECT xp, level FROM users WHERE id = $1'
    const grabXpQueryVals = [id]
    const grabXpQuery = await client.query(grabXpQueryTxt, grabXpQueryVals)
    let currentXp = 0
    let xp = grabXpQuery.rows[0].xp
    let lvl = grabXpQuery.rows[0].level
    if (xp) currentXp = xp
    currentXp += 15

    const checkForLvlUp = checkForLevelUp(currentXp, lvl)
    let addXpQueryTxt = ""
    let addXpQueryVals = []
    if (checkForLvlUp) {
      addXpQueryTxt = 'UPDATE users SET xp = $1, level = $2 WHERE id = $3'
      addXpQueryVals = [checkForLvlUp, lvl + 1, id]
    } else {
      addXpQueryTxt = 'UPDATE users SET xp = $1 WHERE id = $2'
      addXpQueryVals = [currentXp, id]
    }
    const addXpQuery = await client.query(addXpQueryTxt, addXpQueryVals)
  } catch (e) {
    console.log(e)
  } finally {
    client.release()
  }
}

async function checkForUser(id, nickname) {
  const client = await pool.connect()
  try {
    const queryUserIdTxt = 'SELECT id FROM users WHERE id = $1'
    const queryUserIdVals = [id]
    const res = await client.query(queryUserIdTxt, queryUserIdVals)
    if (res.rows.length === 0) createUser(id, nickname)
    else addXp(id)
  } catch (e) {
    console.log(e)
  } finally {
    client.release()
  }
}

router.post('/api/check-for-user', async (req, res) => {
  checkForUser(req.body.id, req.body.nickname)
  res.statusCode = 200
  res.json({
    success: true
  })
})

module.exports = router;