const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
const passport = require('passport')

router.use(bodyParser.json())

router.get('/api/auth/callback', (req, res) => {
    console.log(req.body)
    res.send('auth')
})

module.exports = router;