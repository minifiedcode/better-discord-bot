require('dotenv').config()
const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
const axios = require('axios')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const params = {
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    grant_type: 'authorization_code',
    redirect_uri: "http://localhost:3000/api/auth/discord",
    code: '',
    scope: 'identify'
}

router.use(bodyParser.json())
router.use(cookieParser())
router.use(session({ secret: process.env.SESSION_SECRET, cookie: { maxAge: 900000 } }))

// Authentication uses cookies/session to validate the user over an extended period of time
// This cookie auto expires after 15min

router.get('/api/auth/discord', async (req, res) => {
    if (req.query.code) {
        if (!req.session.access_token) {
            try {
                params.code = req.query.code;
                const response = await axios.post('https://discord.com/api/oauth2/token', new URLSearchParams(params))
                if (response.data.access_token) {
                    req.session.access_token = response.data.access_token;
                }
                res.redirect('/')
            } catch (e) {
                console.log(e)
                res.send({
                    success: false,
                    error: 'Error posting to request token.'
                })
            }
        } else {
            res.redirect('/')
        }

    } else {
        res.send({ success: false })
    }
})

module.exports = router;