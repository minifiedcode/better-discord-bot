// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
const { Pool } = require('pg')
const pool = new Pool()

async function addCmdHistory(cmd, id) {
  const now = new Date();
  const client = await pool.connect()
  try {
    const addCmdQueryTxt = 'INSERT INTO command_logs(command, created_at, created_by) VALUES($1, to_timestamp($2), $3)'
    const addCmdQueryVals = [cmd, now / 1000, id]
    const addCmdQuery = await client.query(addCmdQueryTxt, addCmdQueryVals)
  } catch (e) {
    console.log(e)
    throw e;
  } finally {
    client.release();
  }
}

router.post('/api/add-cmd-history', async (req, res) => {
  addCmdHistory(req.body.id, req.body.cmd)
  res.statusCode = 200
  res.json({
    success: true
  })
})

module.exports = router;
