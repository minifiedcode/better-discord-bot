const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
const { Pool } = require('pg')
const pool = new Pool()

router.use(bodyParser.json())

router.post('/api/get-level', async (req, res) => {
    const client = await pool.connect()
    try {
        const getLevelTxt = 'SELECT level FROM users WHERE id = $1'
        const getLevelVals = [req.body.id]
        const json = await client.query(getLevelTxt, getLevelVals)
        res.send({
            level: json.rows[0].level
        })
    } catch (e) {
        res.send(e)
    }
})

module.exports = router;