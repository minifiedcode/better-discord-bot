import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import React from 'react'

const redirect = encodeURIComponent(`${process.env.NEXT_PUBLIC_API_URL}/api/auth/discord`)
const loginLink = `
https://discord.com/api/oauth2/authorize?client_id=754843484917334108&redirect_uri=${redirect}&response_type=code&scope=identify
`

export default class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {}
    }
  }
  async componentDidMount() {
    try {
      let response = await fetch(`http://${process.env.NEXT_PUBLIC_API_URL}/api/get-user-information`)
      let json = await response.json()
      if (json) {
        this.setState({
          user: json
        })
      }
    } catch (e) {
      throw e;
    }
  }
  render() {
    return (
      <div className={styles.container} >
        <h1>Hi</h1>
        {Object.keys(this.state.user).length === 0 ?
          <Link href={loginLink}><a>Click here to login</a></Link>
          : <p>Welcome back, {this.state.user.username}</p>
        }

      </div>
    )
  }
}

export async function getStaticProps(context) {
  return {
    props: {}
  }
}
