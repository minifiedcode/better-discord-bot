import Head from 'next/head'
import Link from 'next/link'
import React from 'react'

class Login extends React.Component {
  render() {
    return (
      <div>
        <Link href="/"><a>Go back home</a></Link>
        <h1>Login</h1>
      </div>
    )
  }
}

export default Login
