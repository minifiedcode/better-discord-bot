require('dotenv').config()
const fs = require('fs')
const https = require('https')
const express = require('express')
const http = require('http')
const next = require('next')
const cors = require('cors')
const bodyParser = require('body-parser')

const discordAuth = require('./customApi/auth/discord')
const discordCallback = require('./customApi/auth/callback')
const getLevel = require('./customApi/get-level')
const checkForUser = require('./customApi/check-for-user')
const addCmdHistory = require('./customApi/add-cmd-history')
const getUserInformation = require('./customApi/get-user-information')

let privateKey = undefined;
if (fs.existsSync('/etc/letsencrypt/live/immortalcode.net/privkey.pem')) {
    privateKey = fs.readFileSync('/etc/letsencrypt/live/immortalcode.net/privkey.pem', 'utf8');
    const certificate = fs.readFileSync('/etc/letsencrypt/live/immortalcode.net/cert.pem', 'utf8')
    const ca = fs.readFileSync('/etc/letsencrypt/live/immortalcode.net/chain.pem', 'utf8')
}

const dev = process.env.NODE_ENV !== 'production';
const app = next({
    dev
})
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express()
    server.use(bodyParser.urlencoded({ extended: false }))
    server.use(bodyParser.json())
    server.use(discordAuth)
    server.use(discordCallback)
    server.use(getLevel)
    server.use(addCmdHistory)
    server.use(checkForUser)
    server.use(getUserInformation)

    server.use(cors())

    server.get('*', handle)

    http.createServer(server).listen(process.env.PORT, () => {
        console.log(`Listening on port ${process.env.PORT}`)
    })


    if (privateKey) {
        https.createServer({
            key: privateKey,
            cert: certificate,
            ca: ca
        }, server).listen(3001, () => {
            console.log(`Listening on port ${process.env.PORT + 1}`)
        })
    }
})

