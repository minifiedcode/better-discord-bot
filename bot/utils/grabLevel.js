require('dotenv').config()
const fetch = require('node-fetch')

async function grabLevel(id) {
    const body = { id }
    try {
        const res = await fetch(`${process.env.BASEURL}/api/get-level`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        })
    } catch (err) {
        console.log(err)
        return { error: err }
    }

}

module.exports = grabLevel;