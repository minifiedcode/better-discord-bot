const fetch = require('node-fetch')
require('dotenv').config()

async function checkForUser(member) {
    let body = { id: member.id, nickname: member.username }
    await fetch(`${process.env.BASEURL}/api/check-for-user`, {
        headers: {
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(body)
    })
}

module.exports = checkForUser;