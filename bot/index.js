require('dotenv').config()
const Discord = require("discord.js");
const client = new Discord.Client();
const Commands = require('./commands')
const checkForUser = require('./utils/checkForUser')

client.once("ready", () => console.log("Bot is online! :)"));

/*
    Do nothing if message is from the bot
    Check that the proper prefix is being used
*/
client.on('message', (message) => {
    if (message.author.id === client.user.id) return;
    if (!message.content.startsWith("!")) return checkForUser(message.author);
    const cmd = parseCommand(message)
})

/*
    Separates command into an array, dividing the cmds from the parameters
    Check that the command exists
    If it does, perform the command
*/
function parseCommand(message) {
    const messageParts = message.content.split(' ')
    const command = messageParts[0].substring(1)
    const args = messageParts.slice(1)
    if (Commands[command]) {
        //Commands["addCmdHistory"](command, message.author.username)
        Commands[command](message, ...args)
    }
    return message.content;
}
client.login(process.env.CLIENT_TOKEN);