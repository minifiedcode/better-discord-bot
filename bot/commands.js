const Actions = require('./actions')

class Commands {
    static addCmdHistory(cmd, id) {
        Actions.addCmdHistory(cmd, id)
    }

    static yoohoo(message) {
        message.channel.send('Jacob, Yoo-hoo is a superior chocolate MILK. M-I-L-K')
    }

    static pickforme(message, ...args) {
        Actions.pickforme(args)
            .then(choice => message.channel.send(`You should pick ${choice}`))
            .catch(e => message.channel.send(e))
    }

    static geton(message, ...args) {
        Actions.getOn(args)
            .then(mention => {
                // Loop through 5 times and tell the requested user(s) to get on!
                for (let i = 0; i < 5; i++) {
                    message.channel.send(`${mention} someone wants you to get on!!!!`)
                }
            })
            .catch(e => message.channel.send(e))
    }

    static level(message, ...args) {
        Actions.level(message.author.id)
            .then(level => {
                message.channel.send(`You are level ${level}`)
            })
            .catch(e => message.channel.send(e))
    }

    static help(message) {
        const helpMessage = `\`\`\`
Usage: !<command> [value(s)]
Commands:
    yoohoo ........... tells Jacob what's up
    pickforme ........ cycles through however many items you type and picks for you
    geton ............ spams mentioned users 5 times to get on. light 'em up!
    level ............ shows your current level
    faq .............. shows most frequently asked questions
    help ............. displays this text
\`\`\``;
        message.channel.send(helpMessage)
    }

    static faq(message) {
        const helpMessage = `\`\`\`
Q: What does leveling up do?
A: Absolutely nothing Kappa. This feature is TBD

Q: How was this bot made possible?
A: Discord.js, NextJS, and PostgreSQL

Q: Who made this bot?
A: WizzyLodo#0420
\`\`\``;

        message.channel.send(helpMessage)
    }
}
module.exports = Commands;