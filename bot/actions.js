require('dotenv').config()
const grabLevel = require('./utils/grabLevel')
const fetch = require('node-fetch')

class Actions {
    static async addCmdHistory(id, cmd) {
        let body = { id, cmd }
        await fetch(`${process.env.BASEURL}/api/add-cmd-history`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(body)
        })
    }

    static pickforme(...args) {
        return new Promise((resolve, reject) => {
            let stringifyGame = JSON.stringify(args)
            let games = []
            // Run RegExp so that the stringified JSON can be read by bot and can include a formatted response
            stringifyGame = stringifyGame.replace(/[[\]"]/g, "")
            stringifyGame = stringifyGame.replace(/,,/g, ' ')
            games = stringifyGame.split(' ')

            // Remove ',' from entry so that the bot knows which result to choose
            for (let i = 0; i < games.length; i++) {
                games[i] = games[i].replace(',', ' ')
            }

            let random = Math.floor(Math.random() * games.length)
            if (games[random]) {
                resolve(games[random])
            } else {
                reject('Could not find list')
            }
        })
    }

    static getOn(mentions) {
        return new Promise((resolve, reject) => {
            if (Object.keys(mentions).length !== 0) {
                let mentionString = ""
                // Create the long string of mentions
                for (let i = 0; i < mentions.length; i++) {
                    if (i == mentions.length) mentionString += mentions[i]
                    else mentionString += mentions[i] + " "
                }
                resolve(mentionString)
            } else {
                reject('You need to supply a user to mention in the @ format!')
            }
        })
    }

    static level(id) {
        return new Promise((resolve, reject) => {
            fetch(`${process.env.BASEURL}/api/get-level`, {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ id })
            }).then(res => res.json())
                .then(level => resolve(level.level))
                .catch(err => reject(err))
        })
    }
}

module.exports = Actions