require('dotenv').config()
const express = require('express')
const http = require('http')
const next = require('next')

const discordAuth = require('./customApi/auth/discord')
const discordCallback = require('./customApi/auth/callback')

const passport = require('passport')
const session = require('express-session')
const { User } = require('discord.js')
const DiscordStrategy = require('passport-discord').Strategy
    , refresh = require('passport-oauth2-refresh')
var scopes = ['identify']

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

passport.use(new DiscordStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: "/api/auth/callback",
    scopes: scopes
},
    function (accessToken, refreshToken, profile, cb) {
        profile.refreshToken = refreshToken;
        User.findOrCreate({ discordId: profile.id, }, function (err, user) {
            if (err) return done(err)
            return cb(err, user)
        })
    }))

const dev = process.env.NODE_ENV !== 'production';
const app = next({
    dev,
    dir: './'
})
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express()
    server.use(passport.initialize())
    server.use(discordAuth)
    server.use(discordCallback)

    server.get("*", handle)

    http.createServer(server).listen(process.env.PORT, () => {
        console.log(`Listening on port ${process.env.PORT}`)
    })
})